({
    beginThanosFlow : function(component) {
        console.log("Thanos comienza su trabajo.")

        component.set("v.showThanosFlow", true)
        let flow = component.find("flow")
        let inputVariables = [
            {
                name : "PlanetsSelection",
                type : "SObject",
                value : component.get("v.accountRec")
            }
        ]
        flow.startFlow("Thanos_Delete_2", inputVariables)
    },
    beginStrangeFlow : function(component) {
        console.log("Dr. Strange ha visto el futuro y envió un mensaje misterioso...")

        //TODO: Reactivar flow!
        let flow = component.find("strangeflow")
        flow.startFlow("Dr_Strange_Flow")
    },
    callApexController : function(component) {
        const action = component.get("c.getPlanetList")
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                console.log("accountRec - seeSize", response.getReturnValue())
                component.set("v.accountRec", response.getReturnValue())
            }
        })

        $A.enqueueAction(action)
    }
})