({
    doInit : function(component, event, helper) {
        helper.callApexController(component)
    },
    showChat : function(component, event, helper) {
        component.set("v.isButtonDisabled", true)
        component.set("v.showChat", true)

        var modalBody;
        $A.createComponent("c:ThanosModal", {},
            function(content, status) {
                if (status === "SUCCESS") {
                    modalBody = content;
                    component.find('overlayLib').showCustomModal({
                        body: modalBody, 
                        showCloseButton: true,
                        cssClass: "mymodal slds-modal_large",
                        closeCallback: function() {
                            $A.get('e.force:refreshView').fire()
                        }
                    })
                }                               
            }
        );
    },
    statusChange : function(component, event, helper) {
        console.log("OJO - ESTADO CAMBIADO EN FLOW DE THANOS", event)
    },
    strangeStatusChange : function(component, event, helper) {
        //alert("Doctor Strange Flow status changed")
        console.log("OJO - ESTADO CAMBIADO EN FLOW DE STRANGE", event)
    },
    startThanosFlow : function(component, event, helper) {
        console.log("startThanosFlow - Ha llegado el evento")
        //helper.beginThanosFlow(component)
    },
    beginStrangeFlow : function(component, event, helper) {
        //helper.beginStrangeFlow(component)
    },
    reloadPlanetRecords : function(component, event, helper) {
        //helper.callApexController(component)
        console.log("Refresh View")
        $A.get('e.c:FinishActEvent').fire()
    }
})