({
    statusChange : function(component, event, helper) {
        console.log("OJO - ESTADO CAMBIADO EN FLOW DE THANOS", event)
    },
    startThanosFlow : function(component, event, helper) {
        helper.beginThanosFlow(component)
    },
    beginStrangeFlow : function(component, event, helper) {
        console.log("ThanosModalController - BeginStrangeFlow")
        helper.beginStrangeFlow(component)
    },
    doInit : function(component, event, helper) {
        helper.callApexController(component)
    }
})
