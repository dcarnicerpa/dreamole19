({
    doInit : function(component, event, helper) {
        //console.log("Thanos ha hecho su trabajo... ¿Qué será de nosotros ahora?")
        helper.startProgress(component)
    },
    handleValueChange : function(component, event, helper) {
        if(component.get("v.snapProgress") === 10) {
            component.set("v.snapStep", "Thanos ya tiene todas las gemas del infinito en su guante")
        }

        if(component.get("v.snapProgress") === 30) {
            component.set("v.snapStep", "Las gemas están brillando...")
        }

        if (component.get("v.snapProgress") === 50) {
            console.log("Fire StrangeFlowEvent - C")
            component.set("v.snapStep", "El doctor Strange está reaccionando. Parece que está viendo a través del tiempo. Creo que ha lanzado un flow!")
            $A.get("e.c:StrangeFlowEvent").fire()    
        }

        if(component.get("v.snapProgress") === 70) {
            component.set("v.snapStep", "Thanos ha utilizado el poder de las gemas")
        }

        if(component.get("v.snapProgress") === 80) {
            component.set("v.snapStep", "Las personas se están desvaneciendo...")
        }

        if (component.get("v.snapProgress") === 100) {
            console.log("Fire FinishedThanosFlowEvent - C")
            component.set("v.snapStep", "Thanos ha conseguido su cometido. La mitad de la población del universo ha desaparecido. ¿Qué será de nosotros ahora?")
            $A.get("e.c:FinishedThanosFlowEvent").fire()
        }
    }
})