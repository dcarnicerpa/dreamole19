({
    startProgress : function(component) {
        let percentage = setInterval(function(){
            component.set("v.snapProgress", component.get("v.snapProgress") + 1)
            console.log(component.get("v.snapProgress"))
            if (component.get("v.snapProgress") == 100) {
                clearInterval(percentage)
            }
        }, 200)
    }
})