({
    beginSnap : function(component, event, helper) {
        component.set("v.isButtonDisabled", true)
        component.set("v.showSnap", true)
        //Send event to begin flows
        console.log("beginSnap - envio evento beginThanosFlowEvent")
        component.getEvent("beginThanosFlowEvent").fire()
    },
    thanosFlowFinished : function(component, event, helper) {
        console.log("finishedThanosFlowEvent recibido", event)
        component.set("v.showSnapFinished", true)
        //TO DO: darle tiempo
        component.set("v.showFury", true)
        component.set("v.showShield", true)
        component.set("v.showFinish", true)
    },
    strangeFlow : function(component, event, helper) {
        console.log("StrangeFlowEvent recibido", event)
        component.set("v.showStrange", true)
    },
    refreshPage : function(component, event, helper) {
        $A.get('e.force:refreshView').fire()
    }
})