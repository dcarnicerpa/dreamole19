({
    getAftermathResult : function(component, event, helper) {
        const action = component.get("c.getAftermath")
        
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                console.log("planets", response.getReturnValue())
                component.set("v.planets", response.getReturnValue())
            }
        })

        $A.enqueueAction(action)
    },
    flowFinished : function(component, event, helper) {
        component.set("v.showAftermath", true)
    }
})