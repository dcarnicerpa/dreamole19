public without sharing class UndeletePlanetsController {

    /**
     * It undeletes planets in the recycle bin.
     * @author David Carnicer
     */
    @InvocableMethod
    public static void undeletePlanets(){
        List<Account> accs = [SELECT Id, Name FROM Account WHERE IsDeleted = true AND Is_Planet__c = true ALL ROWS];
        if (!accs.isEmpty()) {
            undelete accs;
        }
    }
}