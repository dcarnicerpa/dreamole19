public with sharing class PlanetListController {
    
    @AuraEnabled
    public static Account[] getPlanetList() {
        /*List<Account> accList = [SELECT Id FROM Account WHERE Is_Planet__c = true];
        List<Id> accIds = new List<Id>();

        for (Account a : accList) {
            accIds.add(a.Id);
        }        
        return accIds;*/
        return [SELECT Id, Name FROM Account WHERE Is_Planet__c = true];
    }
}