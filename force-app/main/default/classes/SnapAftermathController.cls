public with sharing class SnapAftermathController {
    
    @AuraEnabled
    public static List<Account> getAftermath() {
        List<Account> accs = new List<Account>([SELECT Id, Name, Number_of_inhabitants__c FROM Account WHERE Number_of_inhabitants__c != null]);
        for (Account a : accs) {
            a.Number_of_inhabitants__c = a.Number_of_inhabitants__c / 2;
        }

        return accs;
    }
}