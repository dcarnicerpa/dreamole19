declare module "@salesforce/apex/PlanetListController.getPlanetList" {
  export default function getPlanetList(): Promise<any>;
}
