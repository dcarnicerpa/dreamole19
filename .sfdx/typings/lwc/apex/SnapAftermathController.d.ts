declare module "@salesforce/apex/SnapAftermathController.getAftermath" {
  export default function getAftermath(): Promise<any>;
}
